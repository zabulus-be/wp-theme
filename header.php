<!doctype html>
<!--[if IE 8]><html class="no-js lt-ie9" <?php language_attributes() ?>><![endif]-->
<!--[if IE 9]><html class="no-js is-ie9" <?php language_attributes() ?>><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes() ?>><!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="format-detection" content="telephone=no">
<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<!-- /end header -->

<div class="container">
    <div class="wrapper">
        <div class="row-24">
            <div class="col-24">
                <nav class="main-menu">
                    <?php wp_nav_menu(array('theme_location' => 'main-menu','menu_class'=> 'main-menu')); ?>
                </nav>
            </div>
        </div>
    </div>
</div>
