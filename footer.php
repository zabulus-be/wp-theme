<div class="container">
    <div class="wrapper">
        <div class="row-24">
            <div class="col-24">
                <nav class="main-menu">
                    <?php wp_nav_menu(array('theme_location' => 'footer-menu','menu_class'=> 'footer-menu')); ?>
                </nav>
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
</body>
</html>
