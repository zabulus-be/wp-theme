<?php
if (!defined('ABSPATH')) exit;
get_header();
?>

<div class="container">
	<div class="wrapper">
		<div class="row-24">
			<div class="col-24">
				<h1>Hoofding 1</h1>
				<h2>Hoofding 2</h2>
				<h3>Hoofding 3</h3>
				<h4>Hoofding 4</h4>
				<p>
					<strong>Tekst in vet</strong><br>
					<strong><em>Tekst in vet en cursief</em></strong><br>
					<em><strong>Tekst in cursief en vet</strong></em><br>
					<em>Tekst in cursief</em>
				</p>
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
				</ul>
				<ol>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
				</ol>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a href="#">Sequi consequuntur</a>, iusto nobis natus expedita fugiat cupiditate, perspiciatis cum nostrum nihil dolore non. Mollitia totam neque accusantium nemo reprehenderit ullam veniam! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem voluptatibus animi, consequuntur aspernatur quam soluta nostrum, quo mollitia ex totam iure provident hic ipsa necessitatibus maxime ratione delectus, cumque sint.
				</p>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
