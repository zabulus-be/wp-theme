<?php

/*================================================
=            Theme features and menus            =
================================================*/

/*
 * Set up features and menus
 * https://codex.wordpress.org/Plugin_API/Action_Reference/after_setup_theme
 */

function zabulus_theme_setup()  {
	// Add theme support for HTML5 Semantic Markup
	// https://codex.wordpress.org/Semantic_Markup
	add_theme_support( 'html5', array( 'search-form', 'caption' ) );

	// Add theme support for document Title tag
	// https://codex.wordpress.org/Title_Tag
	add_theme_support( 'title-tag' );

	// Register navigation menus
	// https://codex.wordpress.org/Function_Reference/register_nav_menu
	// In theme: wp_nav_menu( array('theme_location' => 'main-menu') ) in theme
	register_nav_menu( 'main-menu', __( 'Hoofd menu', 'sassy' ) );
	register_nav_menu( 'footer-menu', __( 'Footer menu', 'sassy' ) );

	// Add theme support for Featured Images
	// https://codex.wordpress.org/Post_Thumbnails
	add_theme_support( 'post-thumbnails' );

	// Add new image sizes
	// https://developer.wordpress.org/reference/functions/add_image_size/
	add_image_size( 'zabulus-thumb-notcropped', 300 ); // 300px width while height follows normal ratio
	add_image_size( 'zabulus-thumb-cropped', 300, 200, true ); // 300px width, 200px height, cropped

}
add_action( 'after_setup_theme', 'zabulus_theme_setup' );

/*=====  End of Theme features and menus  ======*/

/*=========================================================
=            Scripts and styles added to theme            =
=========================================================*/

/*
 * Register and enqueue our theme's style.css
 * afterwards load scripts depending from jQuery
 * 
 * https://codex.wordpress.org/Function_Reference/wp_enqueue_style
 * https://codex.wordpress.org/Function_Reference/wp_deregister_script
 * https://codex.wordpress.org/Function_Reference/wp_enqueue_script
 */

function zabulus_add_scripts() {

	wp_register_style(
		'style', // handle name
		get_bloginfo('stylesheet_directory') . '/style.css', // the URL of the stylesheet
		array(), // not dependent from any style
		filemtime( get_stylesheet_directory() . '/style.css' ), // version
		'all'  // media type
	);
	wp_enqueue_style( 'style' );

	// All scripts referenced by main.js should be compiled together into main.min.js after development
	wp_enqueue_script(
		'main-js',
		get_bloginfo('stylesheet_directory') . '/js/main.js',
		array( 'jquery' ),	// dependent from jquery
		filemtime( get_stylesheet_directory() . '/js/main.js' ), // version
		true 				// $in_footer
	);

}
add_action( 'wp_enqueue_scripts', 'zabulus_add_scripts' );

/*=====  End of Scripts and styles added to theme  ======*/

?>
